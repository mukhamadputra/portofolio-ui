import React from "react";

import "./recentPost.css"
import Rectangle from "./Rectangle";

const article = [
    {
        title: "Making a design system from scratch",
        date: "12 Feb 2020",
        category: "Design, Pattern",
        detail: "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet."
    },
    {
        title: "Creating pixel perfect icons in Figma",
        date: "12 Feb 2020",
        category: "Figma, Icon Design",
        detail: "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet."
    }
]

const RecentPost = () => {
    return (
        <div className="app__recentPost">
            <div className="recentPost__head">
                <li className="col1__title">Recent posts</li>
                <li className="col2__title">View all</li>
                <div className="recentPost__article">
                    <Rectangle arrayData={article} />
                </div>
            </div>
        </div>
    )
}

export default RecentPost;