import React from "react";
import './hero.css'
import image from '../assets/img/hero.png'

const Hero = () => {
    return (
        <div className="app__hero">
            <div className="hero__text">
                <h1 className="hero__title">Hi, I am Jhon,<br />
                    Creative Technologist</h1>
                <p className="hero__article">
                    Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet<br /> 
                    sint. Velit officia consequat duis enim velit mollit. Exercitation veniam<br />
                    consequat sunt nostrud amet.
                </p>
                <button>Download Resume</button>
            </div>
            <div className="hero__img">
                <img src={image} />
            </div>
        </div>

    )
}

export default Hero;