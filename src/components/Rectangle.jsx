import React, { useState } from "react";
import './rectangle.css'

const Rectangle = (props) => {
    const listData = props.arrayData.map((data, index) => {
        return (
            <div className="app__rectangle" key={data.index}>
                <div className="rectangle__content">
                    <p className="rectangle__title">{data.title}</p>
                    <p className="rectangle__log">{data.date} | {data.category}</p>
                    <p className="rectangle__text">{data.detail}</p>
                </div>
            </div>
        )
    })
    return (
        <div className="rectangle__array">{listData}</div>
    )
}

export default Rectangle;