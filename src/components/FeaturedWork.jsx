import React from "react";
import "./featuredWork.css";
import Card from "./Card";

import image1 from "../assets/img/Rectangle 30.png";
import image2 from "../assets/img/Rectangle 32.png";
import image3 from "../assets/img/Rectangle 34.png";

const article = [
    {
        image: image1,
        title: "Designing Dashboards",
        year: "2020",
        category: "Dashboard",
        text: "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet."
    },
    {
        image: image2,
        title: "Vibrant Portraits of 2020",
        year: "2018",
        category: "Illustration",
        text: "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet."
    },
    {
        image: image3,
        title: "36 Days of Malayalam type",
        year: "2018",
        category: "Typography",
        text: "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet."
    }
]

const FeaturedWork = () => {
    return (
        <div className="app__featuredWork">
            <p className="featuredWork__title">Featured works</p>
            <Card arrayData={article}/>
        </div>
    )
}

export default FeaturedWork;