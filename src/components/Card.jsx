import React, { useState } from "react";
import "./card.css";

const Card = (props) => {
    const listData = props.arrayData.map((data, index) => {
        return (
            <div className="app__card" key={data.index}>
                <div className="card__box">
                    <div className="card__img">
                        <img src={data.image} />
                    </div>
                    <div className="card__text">
                        <p className="card__title">{data.title}</p>
                        <p className="card__year">
                            <span className="year__text">{data.year}</span>
                            <span className="card__category">{data.category}</span>
                        </p>
                        <p className="card__article">{data.text}</p>
                    </div>
                </div>
            </div>
        )
    })
    return (
        <div>{listData}</div>
    )
}

export default Card;