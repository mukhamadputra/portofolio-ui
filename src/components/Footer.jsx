import React from "react";
import "./footer.css";

import fb from "../assets/img/fb.png";
import ig from "../assets/img/ig.png";
import tweet from "../assets/img/tweet.png";
import linked from "../assets/img/linked.png";

const Footer = () => {
    return (
        <div className="app__footer">
            <div className="footer__icon">
                <img src={fb} />
                <img src={ig} />
                <img src={tweet} />
                <img src={linked} />
            </div>
            <div className="footer__copy">
                <p>Copyright ©2020 All rights reserved </p>
            </div>
        </div>
    )
}

export default Footer;