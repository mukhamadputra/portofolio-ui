import React from 'react';
import './navbar.css';

const Navbar = () => {
    return (
        <div className='app__navbar'>
            <ul className='navbar__menu'>
                <li><a className='navbar__link'>Works</a></li>
                <li><a className='navbar__link'>Blog</a></li>
                <li><a className='navbar__link'>Contact</a></li>
            </ul>
        </div>
    );
}

export default Navbar;