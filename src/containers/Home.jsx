import React from "react";

import "./home.css"
import FeaturedWork from "../components/FeaturedWork";
import Footer from "../components/Footer";
import Hero from "../components/Hero";
import Navbar from "../components/Navbar";
import RecentPost from "../components/RecentPost";

const Home = () => {
    return (
        <div className="app__home">
            <Navbar />
            <Hero />
            <RecentPost />
            <FeaturedWork />
            <Footer />
        </div>
    )
}

export default Home;